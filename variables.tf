variable "region" {
  default = "eu-west-3"
}

variable "instance_name" {
  description = "Value of the Name tag for the EC2 instance"
  type        = string
  default     = "ExampleAppServerInstanceVariable"
}
