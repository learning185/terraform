terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "terraform"
  region  = var.region
}

resource "aws_instance" "app_server" {
  ami           = "ami-0f79604849d0fcaab"
  instance_type = "t2.micro"

  tags = {
    Name = var.instance_name
  }
}
